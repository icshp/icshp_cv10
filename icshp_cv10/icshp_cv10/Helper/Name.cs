﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace icshp_cv10.Helper
{
    static class Name
    {
        private static Stack<string> _alphabet;

        private static int _count;
        static Name()
        {
            _alphabet = new Stack<string>();
            _count = 0;
            CreateStackOfAlphabet();
        }

        public static void CreateStackOfAlphabet()
        {
            char[] abc = "zyxwvutsrqponmlkjihgfedcba".ToCharArray();

            //Malé
            foreach (char item in abc)
            {
                _alphabet.Push(item.ToString());
            }

            //Velké
            foreach (char item in abc)
            {
                _alphabet.Push(item.ToString().ToUpper());
            }
        }

        public static string GetName()
        {
            if(_alphabet.Count != 0)
            {
                if(_count > 0)
                {
                    return _count.ToString() + _alphabet.Pop();
                } else
                {
                    return _alphabet.Pop();
                }
            } else
            {
                CreateStackOfAlphabet();
                _count++;
                return GetName();
            }
        }
    }
}
