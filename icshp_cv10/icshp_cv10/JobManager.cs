﻿using icshp_cv10.Helper;
using icshp_cv10.Models;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography;
using System.Text;

namespace icshp_cv10
{
    class JobManager
    {
        public List<Job> Jobs { get; private set; }

        public Job[,] Scheduler { get; }

        private const int machines = 8;
        private const int time = 100;

        public JobManager()
        {
            Jobs = new List<Job>();
            Scheduler = new Job[machines, time];
        }

        public void ReadFiles()
        {
            try
            {
                foreach (string path in GetFilesInDirectory())
                {
                    string[] lines = System.IO.File.ReadAllLines(path);
                    foreach (string item in lines)
                    {
                        Jobs.Add(new Job(item, path));
                    }
                }
                Jobs = Jobs.OrderBy(o => o.CreateDate).ToList();
                AssignNames();
                CreateSchedule();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<int> GetAdditionalTaskSize()
        {
            List<List<Models.Range>> ranges = GetRange();
            List<int> matrix = new List<int>();

            for (int i = 0; i < machines; i++)
            {
                Models.Range range = ranges[i].Last();
                matrix.Add(range.Size());
            }

            matrix.Sort();
            matrix.Reverse();

            return matrix;
        }

        private List<List<Models.Range>> GetRange()
        {
            List<List<Models.Range>> ranges = new List<List<Models.Range>>();
            for (int x = 0; x < Scheduler.GetLength(0); x++)
            {
                ranges.Add(CheckMachine(x));
            }
            return ranges;
        }

        private void CreateSchedule()
        {
            foreach (Job job in Jobs)
            {
                List<List<Models.Range>> ranges = GetRange();

                bool[] machines = new bool[Scheduler.GetLength(0)];
                int count = 0, y;
                for (y = 0; y < Scheduler.GetLength(1); y++)
                {
                    machines = new bool[Scheduler.GetLength(0)];
                    count = 0;
                    for (int i = 0; i < ranges.Count; i++)
                    {
                        int yy = 0;
                        foreach (Models.Range range in ranges[i])
                        {
                            if (range.InRange(y, job.Duration))
                            {
                                if (yy == 0)
                                {
                                    machines[i] = true;
                                    count++;
                                    yy++;
                                }
                            }
                        }
                    }
                    if (count >= job.Shares)
                    {
                        break;
                    }
                }

                int counter = 0;
                for (int i = 0; i < machines.Length; i++)
                {
                    if (machines[i])
                    {
                        counter++;
                        AddToSchedule(job, y, i);
                    }
                    if (counter == job.Shares)
                    {
                        break;
                    }
                }
            }
        }

        private void AssignNames()
        {
            foreach (Job item in Jobs)
            {
                item.JobName = Name.GetName();
            }
        }

        private void AddToSchedule(Job job, int start, int machine)
        {
            for (int i = start; i < job.Duration + start; i++)
            {
                Scheduler[machine, i] = job;
            }
        }

        private List<Models.Range> CheckMachine(int machine)
        {
            List<Models.Range> range = new List<Models.Range>();
            int min = -1;
            int max = -1;
            for (int i = 0; i < Scheduler.GetLength(1); i++)
            {
                if (Scheduler[machine, i] == null)
                {
                    if (min == -1)
                    {
                        min = i;
                    }
                    if (max < i)
                    {
                        max = i;
                    }
                }

                if (Scheduler[machine, i] != null || i == Scheduler.GetLength(1) - 1)
                {
                    if (min != -1 && max != -1)
                    {
                        range.Add(new Models.Range(min, max));
                        min = -1;
                        max = -1;
                    }
                }
            }
            return range;
        }

        private string[] GetFilesInDirectory()
        {
            string[] paths;
            try
            {
                paths = Directory.GetFiles(@"planner\", "*.jobs");
                return paths;
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }
    }
}
