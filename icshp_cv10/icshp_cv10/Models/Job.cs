﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace icshp_cv10.Models
{
    class Job
    {
        public string JobName { get; set; }
        int U { get; }
        public int Shares { get; }
        public int Duration { get; }

        public int CreateDate { get; }

        public Job(string line, string fileName)
        {
            try
            {
                string[] data = line.Split(',');
                U = GetUserID(fileName);
                CreateDate = Convert.ToInt32(data[1]);
                Shares = Convert.ToInt32(data[2]);
                Duration = Convert.ToInt32(data[3]);
            }
            catch (Exception)
            {
                throw new ArgumentException("Vstupní data jsou nesprávná!");
            }
        }

        private int GetUserID(string fileName)
        {
            try
            {
                if (fileName.Contains("jobs"))
                {
                    return Convert.ToInt32(Regex.Match(fileName, @"\d+").Value);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {

                throw new ArgumentException("Název soubrou není správný!");
            }
        }

        public override string ToString()
        {
            return String.Format("{0} (u: {1,1:D}, shares: {2,1:D}, duration: {3,2:D}, createdat: {4,1:D})",JobName, U, Shares, Duration, CreateDate);
        }
    }
}
