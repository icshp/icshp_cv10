﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Dynamic;
using System.Text;

namespace icshp_cv10.Models
{
    class Range
    {
        public int start {get; }
        private int stop;

        public Range(int min, int max)
        {
            start = min;
            stop = max;
        }

        public bool InRange(int num) {
            return (num <= stop && num >= start);
        }

        public bool InRange(int startIndex, int size)
        {
            if(startIndex == -1)
            {
                return ((stop - start) - size) >= 0;
            } else
            {
                if (start <= startIndex) {
                    if (start == stop && size == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return ((stop - (startIndex-1)) - size) >= 0;
                    }
                } else {
                    return false;
                }
            }
        }

        public int Size()
        {
            return stop - start + 1;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", start, stop);
        }
    }
}
