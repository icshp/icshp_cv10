﻿using icshp_cv10.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace icshp_cv10
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                JobManager jobManager = new JobManager();
                jobManager.ReadFiles();
                PrintJobs(jobManager);
                PrintSchedule(jobManager);
                PrintAdditionalJobs(jobManager);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void PrintJobs(JobManager jb)
        {
            Console.WriteLine("Úlohy:");
            foreach (Job item in jb.Jobs)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public static void PrintAdditionalJobs(JobManager jb)
        {
            Console.WriteLine("Doplňující úlohy:");
            List<int> matrix = jb.GetAdditionalTaskSize();
            for (int i = 0; i < matrix.Count; i++)
            {
                Console.WriteLine(String.Format("Nejdelší úloha pro {0} strojů je {1}", i + 1, matrix[i]));
            }
        }

        public static void PrintSchedule(JobManager jb)
        {
            Console.WriteLine("Naplánované úlohy:");
            for (int i = 0; i < jb.Scheduler.GetLength(0); i++)
            {
                Console.Write(i + " ");
                for (int y = 0; y < jb.Scheduler.GetLength(1); y++)
                {
                    if (jb.Scheduler[i, y] == null)
                    {
                        Console.Write("-");
                    }
                    else
                    {
                        Console.Write(jb.Scheduler[i, y].JobName);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
